<?php

$dec = 43261596;
$bin = base_convert($dec,10,2);
while (strlen((string)$bin) < 32) {
	$bin = "0".$bin;
}

$newBin = str_split($bin);

$reverseBin = array_reverse($newBin);

$reverseBin = implode("", $reverseBin);

$reverseDec = base_convert($reverseBin, 2, 10);
echo $reverseDec;